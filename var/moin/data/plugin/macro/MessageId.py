from MoinMoin import wikiutil

def execute(macro, id):
	url = u'https://lists.debian.org/msgid-search/%(id)s' % {'id': id}
	return \
		macro.formatter.text('<') + \
		macro.formatter.url(1, url) + \
		macro.formatter.text(id) + \
		macro.formatter.url(0) + \
		macro.formatter.text('>')
