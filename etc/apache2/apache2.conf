# vim: set ts=2 sw=2 et si sm:

# Needs to be a UTF-8 locale so that python will talk sensibly to
# spawned children like the account creation check script
SetEnv LANG C.UTF-8

WSGIDaemonProcess wiki.debian.org user=wikiweb group=wikiweb home=/srv/wiki.debian.org/home-unpriv processes=16 threads=16 maximum-requests=1000 umask=0007 display-name=wsgi-wiki.debian.org

Use common-debian-service-https-redirect * wiki.debian.org

<VirtualHost *:443>
  ServerName wiki.debian.org
  ServerAdmin wiki@debian.org

  ErrorLog  /var/log/apache2/wiki.debian.org-error.log
  CustomLog /var/log/apache2/wiki.debian.org-access.log combined

  Use common-debian-service-ssl wiki.debian.org
  Use common-ssl-HSTS

  # Lots of junk lookups for files that have never existed. Try to
  # make them fail faster
  # Cyrillic for "WikiCourse"
  AliasMatch ^/%D0%92%D0%B8%D0%BA%D0%B8%D0%9A%D1%83%D1%80%D1%81 /srv/wiki.debian.org/NONEXISTENT 
  # Apple browser junk?
  AliasMatch ^/apple-touch-icon /srv/wiki.debian.org/NONEXISTENT
  # Morons probing for WordPress or PHP exploits
  AliasMatch ^/.*\.php /srv/wiki.debian.org/NONEXISTENT
  # WTF?
  AliasMatch ^/LDAP/NSS?action=newaccount /srv/wiki.debian.org/NONEXISTENT
  AliasMatch ^/LDAP/NSS?action=login /srv/wiki.debian.org/NONEXISTENT

  Alias /htdocs      /srv/wiki.debian.org/usr/htdocs
  Alias /robots.txt  /srv/wiki.debian.org/usr/htdocs/robots.txt
  Alias /favicon.ico /srv/wiki.debian.org/usr/htdocs/favicon.ico

# This alias needs to be updated on wiki upgrade, according to "release_short" in
# /usr/share/pyshared/MoinMoin/version.py
  Alias /moin_static193/debwiki    /srv/wiki.debian.org/usr/htdocs/debwiki/
  Alias /moin_static193    /usr/share/moin/htdocs/
  ExpiresActive On
  <Directory /usr/share/moin/htdocs>
    require all granted
    # Moinmoin use file-versioning, so we can cache agressively.
    ExpiresDefault "access plus 1 week"
  </Directory>

  ScriptAlias /cgi-bin/bugstatus /srv/wiki.debian.org/bin/bugstatus
  WSGIScriptAlias /cgi-bin/launchpad /srv/wiki.debian.org/bin/launchpad

  CacheEnable mem /

  <Directory /srv/wiki.debian.org/usr/htdocs>
    require all granted
    ExpiresDefault "access plus 1 day"
  </Directory>

  # Massive numbers of failures from a few referers, let's just drop them
  SetEnvIf Referer "osakos\.com" banned_referer
  SetEnvIf Referer "aff-redir\.com" banned_referer
  SetEnvIf Referer "zoomquiet\.top" banned_referer
  SetEnvIf Referer "nuriadejuan\.com" banned_referer
  SetEnvIf Referer "quecall\.cl" banned_referer
  SetEnvIf Referer "tinylink\.in" banned_referer
  SetEnvIf Referer "cnsvabogados\.com" banned_referer
  SetEnvIf Referer "dangkysv388\.com" banned_referer
  SetEnvIf Referer "squarelovin\.com" banned_referer
  SetEnvIf Referer "tritechsc\.ksd\.org" banned_referer
  # test!
  # SetEnvIf Referer "einval\.com" banned_referer

  WSGIScriptAlias / /srv/wiki.debian.org/bin/moin.wsgi
  WSGIProcessGroup wiki.debian.org
  WSGIPassAuthorization On
  <Location />
    <RequireAll>
      require all granted
      require not env banned_referer
      # polling many *many* times
      require not ip 149.249.244.126
    </RequireAll>
  </Location>
</VirtualHost>
